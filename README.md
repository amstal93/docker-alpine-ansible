# Docker Image: Alpine Linux Ansible

This project uses [Packer](https://packer.io) to build a [Docker](https://www.docker.com)  image based on the latest version of [Alpine Linux](https://www.alpinelinux.org/) and store it in this project's GitLab container registry.

It includes all of the software that you should need to test [Ansible](https://www.ansible.com) [roles](https://docs.ansible.com/ansible/latest/user_guide/playbooks_reuse_roles.html) during development.

  - Ansible
  - Docker
  - [Molecule](https://molecule.readthedocs.io/en/stable/), a tool to test applying Ansible roles on different platforms.
  - [molecule-docker](https://github.com/ansible-community/molecule-docker/), a Molecule package that supports running tests on Docker containers.
  - [Tox](https://tox.readthedocs.io/en/latest/), a tool to test applying Ansible roles using different versions of Ansible.
  
This project was created so that I could create [GitLab CI](https://docs.gitlab.com/ee/ci/) pipelines in Ansible roles that I develop. This image spins up other Docker images that are used for Molecule and Tox tests.

## Release

See the [container registry page](https://gitlab.com/centinel-foss/docker-alpine-ansible/container_registry) for a list of available container versions.

See the [releases page](https://gitlab.com/centinel-foss/docker-alpine-ansible/-/releases) for a detailed changelog for container versions.

## How to Use

1. Sign up for a free account on [GitLab.com](https://gitlab.com).
2. Create a [new project](https://docs.gitlab.com/ee/gitlab-basics/create-project.html).
3. Develop your Ansible role.
4. Create a `.gitlab-ci.yml` file at the root level of your project directory that uses this image in the `image` setting and runs Molecule and Tox tests in the `script` section.

See my Ansible project [repositories](https://gitlab.com/centinel-foss) for examples. 

## How to Contribute

This project uses GitLab's built-in management tools to track project versions. See the sections below for a step-by-step walkthrough.

### Project Versions

Project versions follow the syntax `major.minor.patch` - for instance, `1.2.3`. Before making changes to the project, you'll have to determine what the next version of the project should be.

* `major` (`1` in the example): Increment this for breaking changes that are not backward compatible.
  * **Example**: Completely redesigning the project's Docker container.
* `minor` (`2` in the example): Increment this for significant changes that don't affect backward compatibility.
  * **Examples**: Updating Terraform/Terragrunt to a new non-patch release (I.E. `v0.26.7` to `v0.27.0`) 
* `patch` (`3` in the example): Increment this for trivial changes that shouldn't affect the built Docker container.
  * **Examples**: Changes to documentation; updating Terraform/Terragrunt to new patch releases (I.E. `v0.26.7` to `v0.26.8`).

### GitLab Milestones

Create a [GitLab Milestone](https://docs.gitlab.com/ee/user/project/milestones/) in this repository named after the new version. For instance, if you've decided that the next version of the project should be `1.10.0`, call the milesone `1.10.0`.

In the `Description` field, include a summary of changes that will be introduced with the new version as summarized in three headers: `Add`, `Change`, and `Remove`. Include bullet points under each of these that detail the changes.

Just as with Git commit messages, write in the present tense.

### GitLab Issues

Go to the project's [GitLab issues](https://docs.gitlab.com/ee/user/project/issues/). Assign existing issues to the milestone you created as appropriate.

If some of your planned features don't have corresponding GitLab issues yet, then create issues for them and then assign them to the milestone.

### Completion

Complete work on all GitLab issues associated with the milestone. 

Go to the project's [Releases page](https://gitlab.mimsoftware.com/orion/containers/terragrunt-container/-/releases). Create a new release with the same name as the milestone. In the `Release notes` section, paste the same notes you wrote for the GitLab milestone. 

Once your new version has been saved, GitLab CI will build a new version of the Docker container and push it to the project's Docker registry.

Finally, pull up the milestone in GitLab and mark it as `closed`.

## See Also

  * *[What is Docker?](https://opensource.com/resources/what-docker)*: A introductory article that goes over the basics of Docker.
  * *[Infrastructure Testing with Molecule](https://www.ansible.com/infrastructure-testing-with-molecule)*: A video that describes how you can use Molecule to run automated tests on Ansible roles you're developing.

## Support

If you need further assistance, please submit a [GitLab issue](https://gitlab.com/centinel-foss/docker-alpine-ansible/-/issues) in this repository.

## License

Apache License, 2.0.

## Author Information

Created in 2019 by [Justin Smith](mailto:justin@adminix.net).
