# Source Settings
# ===============
# This file supplies Packer with the configuration
# settings that it needs to build a Docker image.

source "docker" "docker_container" {
  image = "docker:${var.docker_version}"
  # After the Docker container is built, commit it
  # to the build environment's Docker registry so that
  # it can be pushed.
  commit = true
}
