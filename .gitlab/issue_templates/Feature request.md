# Feature Request

## What is your feature request?
*Use checkboxes* `[ ]` *to list multiple items if necessary.*


## What is the expected value of your request?


## Development plan
*This section should only be filled out by the developer(s). Divide your work up into one or more batches (smaller portions) and list them below. Assign each batch to a person by @mentioning their name.*

*Each batch should have its own merge request. Don't close this issue until all batches are done.*

| Batch | Owner |
| ------ | ------ |
| do this thing | @username |

/label ~"Category: Feature" ~"Severity: 2"
/milestone %Backlog
