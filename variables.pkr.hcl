# Input Variables
# ===============
# These values are expected to be passed in as environment
# variables of the format PKR_VAR_varname unless otherwise
# specified.

variable "docker_repository" {
  type = string
  description = "The Docker repository where the built image should be pushed."
}

variable "docker_tag" {
  type = string
  description = "The tag that should be attached to the built image."
}

variable "docker_version" {
  type = string
  description = "The version of Docker to be present on the container."
}

variable "python_ansible_version" {
  type = string
  description = "The major version of the 'ansible' Python package to be installed."
}

variable "python_docker_version" {
  type = string
  description = "The major version of the 'docker' Python package to be installed."
}

variable "python_molecule_version" {
  type = string
  description = "The major version of the 'molecule' Python package to be installed."
}

variable "python_molecule_docker_version" {
  type = string
  description = "The major version of the 'molecule-docker' Python package to be installed."
}

variable "python_tox_version" {
  type = string
  description = "The major versio of the 'tox' Python package to be installed."
}
